local nord = {
	--16 colors
	black = "#101010", -- nord0 in palette
	dark_gray = "#101010", -- nord1 in palette
	gray = "#434C5E", -- nord2 in palette
	light_gray = "#525252", -- nord3 in palette
	light_gray_bright = "#616E88", -- out of palette
	darkest_white = "#b9b9b9", -- nord4 in palette
	darker_white = "#b9b9b9", -- nord5 in palette
	white = "#f7f7f7", -- nord6 in palette
	teal = "#868686", -- nord7 in palette
	off_blue = "#868686", -- nord8 in palette
	glacier = "#686868", -- nord9 in palette
	blue = "#5E81AC", -- nord10 in palette
	red = "#7c7c7c", -- nord11 in palette
	orange = "#D08770", -- nord12 in palette
	yellow = "#a0a0a0", -- nord13 in palette
	green = "#8e8e8e", -- nord14 in palette
	purple = "#747474", -- nord15 in palette
	none = "NONE",
}
return nord
